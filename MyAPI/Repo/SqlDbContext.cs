﻿using Microsoft.EntityFrameworkCore;
using MyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAPI.Repo
{
    public class SqlDbContext :DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public SqlDbContext(DbContextOptions options):base(options)
        {

        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer();
        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
