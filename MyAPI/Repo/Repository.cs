﻿using MyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyAPI.Repo;

namespace MyAPI.Repo
{
    public class EmployeeRepository : IRepository
    {
        public List<Employee> _employees;
        public SqlDbContext _context;
        EmployeeRepository(List<Employee> employees, SqlDbContext context)
        {
            _employees = employees;
            _context = context;
        }

        public bool AddEmployee(Employee employee)
        {
            try
            {
                _context.Employees.Add(employee);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }


        }

        public bool DeleteEmployee(Employee employee)
        {

            try
            {
                _context.Employees.Remove(employee);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public Employee GetEmployee(int EmployeeId)
        {
            try
            {
                return _context.Employees?.FirstOrDefault(a => a.Employeeid == EmployeeId);
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        public List<Employee> GetEployees()
        {
            return _context.Employees.ToList();
        }

        public bool UpdateEmployee(Employee employee)
        {

            try
            {
                Employee employeeToUpdate = _context.Employees.FirstOrDefault(a => a.Employeeid == employee.Employeeid);

                employeeToUpdate.EmployeeLName = employee.EmployeeLName;
                employeeToUpdate.EmployeeFName = employee.EmployeeFName;
                employeeToUpdate.EmployeeAddress = employee.EmployeeAddress;
                employeeToUpdate.EmployeeSalary = employee.EmployeeSalary;
                _context.Update(employeeToUpdate);
                _context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
