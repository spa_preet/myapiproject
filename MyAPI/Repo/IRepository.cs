﻿using MyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAPI.Repo
{
    public interface IRepository
    {
        List<Employee> GetEployees();
        Employee GetEmployee(int EmployeeId);
        bool AddEmployee(Employee employee);
        bool UpdateEmployee(Employee employee);
        bool DeleteEmployee(Employee employee);




    }
}
