﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAPI.Model
{
    public class Employee
    {
        public int Employeeid { get; set; }
        public string EmployeeFName { get; set; }
        public string EmployeeLName { get; set; }
        public string EmployeeAddress { get; set; }
        public int EmployeeSalary { get; set; }
    }
}
