﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyAPI.Model;
using MyAPI.Repo;

namespace MyAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<Employee> _logger;
        public SqlDbContext _context { get; set; }

        public EmployeeController(ILogger<Employee> logger, SqlDbContext context)
        {
            _logger = logger;
            _context = context;

        }

        [HttpGet("GetEmployees")]
        public IEnumerable<Employee> Get()
        {
            return _context.Employees;

        }
        [HttpGet("GetEmployee")]
        public Employee GetSingle(int id)
        {
            return _context.Employees.FirstOrDefault(a => a.Employeeid == id);
        }
        [HttpPost("AddEmployee")]
        public Boolean Post(Employee employee)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(employee.EmployeeFName) && !string.IsNullOrWhiteSpace(employee.EmployeeLName))
                _context.Employees.Add(employee);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPut("EditEmployee")]
        public Boolean EditEmployee(Employee employee)
        {

            try
            {
                if (employee.Employeeid != null)
                {
                    var Employee = _context.Employees.SingleOrDefault<Employee>(a => a.Employeeid == employee.Employeeid);
                    if (Employee != null)
                    {
                        Employee.EmployeeFName = employee.EmployeeFName;
                        Employee.EmployeeLName = employee.EmployeeLName;
                        Employee.EmployeeAddress = employee.EmployeeAddress;
                        Employee.EmployeeSalary = employee.EmployeeSalary;
                    }
                    _context.Employees.Update(Employee);
                    _context.SaveChanges();
                    return true;

                }
                return false;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpDelete("DeleteEmployee")]
        public bool Delete(int EmployeeId)
        {
            try
            {
                var Employee = _context.Employees.FirstOrDefault(a => a.Employeeid == EmployeeId);
                if (Employee != null)
                {
                    _context.Employees.Remove(Employee);
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}


