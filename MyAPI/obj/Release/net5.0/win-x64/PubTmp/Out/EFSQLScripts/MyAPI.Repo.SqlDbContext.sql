﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211119173751_AddEmployeeTodb')
BEGIN
    CREATE TABLE [Employees] (
        [Employeeid] int NOT NULL IDENTITY,
        [EmployeeFName] nvarchar(max) NULL,
        [EmployeeLName] nvarchar(max) NULL,
        [EmployeeAddress] nvarchar(max) NULL,
        [EmployeeSalary] int NOT NULL,
        CONSTRAINT [PK_Employees] PRIMARY KEY ([Employeeid])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211119173751_AddEmployeeTodb')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20211119173751_AddEmployeeTodb', N'5.0.12');
END;
GO

COMMIT;
GO

